#!/usr/bin/python3
#DESCRIPTION: Entry point for Wetlens
#Tyler - Crimsonthinker
import sys
from PyQt5 import QtWidgets
from source.ui.MainWindow import WLMainWindow

def main():
    app = QtWidgets.QApplication(sys.argv)
    gm = WLMainWindow()
    gm.showMaximized()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()