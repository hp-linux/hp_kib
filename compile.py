from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
import subprocess
import sys

#VARIABLE PATH
ROOTPATH = sys.path[0]
BUILDPATH = sys.path[0] + '/build'
MAINFILEPYROOTPATH = 'forms/main/forms_py'
MENUFILEPYROOTPATH = 'forms/menu_objects/menu_file_objects/forms_py'
RCROOTPATH = 'forms/resources'
SRCPATH = 'source'
MDLPATH = 'modules'

#RUN MAKEFILE FOR COMPILING UI FILE TO PYTHON FILE
subprocess.call(['make build'],shell=True)

#COMPILING PYTHON FILE TO BINARY ONES

ext_modules = [
    Extension('resource_rc',[RCROOTPATH + '/resource_rc.py']),
    Extension('tool',[MDLPATH + '/tool.py']),
    Extension('GMWelComeScreen',[MAINFILEPYROOTPATH + '/GMWelcomeScreen.py']),
    Extension('GMMainWidget',[MAINFILEPYROOTPATH + '/GMMainWidget.py']),
    Extension('MainWindow',[MAINFILEPYROOTPATH + '/MainWindow.py']),
    Extension('ShowMagnificationDialog',[MAINFILEPYROOTPATH + '/ShowMagnificationDialog.py']),
    Extension('OpenProjectDialog',[MENUFILEPYROOTPATH + '/OpenProjectDialog.py']),
    Extension('GMSysConf',[SRCPATH + '/GMSysConf.py']),
    Extension('GMImageViewer',[SRCPATH + '/GMImageViewer.py']),
    Extension('OpenProjectDialog',[SRCPATH + '/OpenProjectDialog.py']),
    Extension('ShowMagnificationDialog',[SRCPATH + '/ShowMagnificationDialog.py']),
    Extension('MainWindow',[SRCPATH + '/MainWindow.py'])
]

setup(
    name='Wetlens',
    cmdclass = {'build_ext' : build_ext},
    ext_modules = ext_modules
)