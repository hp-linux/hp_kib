from PyQt5 import QtWidgets

class MessageBox(QtWidgets.QMessageBox):
    def __init__(self,typeMessageBox,parent=None,title=None,mainMessage=None,informativeText=None,detailedText=None):
        super(MessageBox,self).__init__(parent=parent)
        self.setIcon(typeMessageBox)
        if title is not None:
            self.setWindowTitle(title)
        if mainMessage is not None:
            self.setText(mainMessage)
        if informativeText is not None:
            self.setInformativeText(informativeText)
        if detailedText is not None:
            self.setDetailedText(detailedText)
        self.setStandardButtons(QtWidgets.QMessageBox.Ok)