#pragma once
#include <WLEdgeDetector.hpp>
#include <WLFilter.hpp>
#include <WLSeg.hpp>
#include <WLImage.hpp>
#include <WLRegion.hpp>
#include <boost/python/object.hpp>
#include <boost/python/list.hpp>
#include <boost/python.hpp>
#include <Python.h>
#include <string>
#include <vector>
#include <map>

#if !defined CV_VERSION_EPOCH && CV_VERSION_MAJOR == 4
namespace WLFunc{
}
#endif