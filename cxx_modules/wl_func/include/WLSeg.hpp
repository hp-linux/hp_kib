#pragma once
#include <opencv4/opencv2/core.hpp>
#include <opencv4/opencv2/highgui.hpp>
#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/imgcodecs.hpp>
#include <opencv4/opencv2/core/matx.hpp>
#include <string>
#include <math.h>
#include <WLFilter.hpp>
#include <WLImage.hpp>
#include <WLRegion.hpp>

namespace WLFunc{
    struct WLSeg{
        private:
        static bool _checkConnectedPath(const cv::Mat &image,int rows, int cols, int x, int y, int currentLength, int maxLength);
        static bool _divideRegion(cv::Mat &inputRegion,cv::Mat &outputRegion, int x, int y, int connectivity);

        public:
        static bool hysteresisThreshold(WLObject::WLImage *const inputImage, WLObject::WLRegion *outputRegion,
        int low, int high, int maxLength);
        static bool thresholdWithBoundary(WLObject::WLImage *inputImage, WLObject::WLRegion *outputRegion, int thresholdVal, int maxThresholdVal);
        static bool threshold(WLObject::WLImage *inputImage, WLObject::WLRegion *outputRegion, int thresholdVal);
        static bool divideConnectedRegion(WLObject::WLRegion *inputRegion,WLObject::WLRegion *outputRegion, int idx, int connectivity);
    };
}