#pragma once

#include <opencv4/opencv2/core.hpp>
#include <opencv4/opencv2/highgui.hpp>
#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/imgcodecs.hpp>
#include <opencv4/opencv2/core/matx.hpp>
#include <WLImage.hpp>

namespace WLFunc{
    struct WLFilter{
        private:
        static const float SIGMA_3, SIGMA_5, SIGMA_7, SIGMA_9, SIGMA_11;
        static double binomialCoefficient(unsigned long long l, unsigned long long k);
        public:
        //filters that takes input as cv::Mat will not be involved in scaling and conversion of type (except WLImage)
        static bool _gaussianFilter(cv::Mat &image, cv::Mat &outputImage,int size);
        static bool gaussianFilter(WLObject::WLImage *image, WLObject::WLImage *outImage, int size);
        static bool _binomialFilter(cv::Mat &image, cv::Mat &outputImage, int width, int height);
        static bool binomialFilter(WLObject::WLImage *image, WLObject::WLImage *outImage, int width, int height);
    };
    
}