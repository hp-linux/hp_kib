#pragma once
#include <opencv4/opencv2/core.hpp>
#include <opencv4/opencv2/highgui.hpp>
#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/imgcodecs.hpp>
#include <opencv4/opencv2/core/matx.hpp>
#include <string>
#include <math.h>
#include <WLFilter.hpp>
#include <WLImage.hpp>

namespace WLFunc{
    //constant for sobel edge detector
    struct WLEdgeDetector{
        public:
        static const std::string GAUSSIAN_FILTER;
        static const std::string BINOMIAL_FILTER;

        static const std::string SUM_ABS;
        static const std::string SUM_SQRT;
        static const std::string SUM_ABS_ON_DIRECTION;
        static const std::string MAX_ABS_ON_DIRECTION;
        static const std::string X;
        static const std::string Y;

        static bool sobelAmplitude(WLObject::WLImage *const inputImage, WLObject::WLImage *outputImage,
        std::string type, std::string filterType, int filterSize);
    };
    
}