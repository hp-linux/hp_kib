#include <WLFilter.hpp>

namespace WLFunc{
    const float WLFilter::SIGMA_3 = 0.600;
    const float WLFilter::SIGMA_5 = 1.075;
    const float WLFilter::SIGMA_7 = 1.550;
    const float WLFilter::SIGMA_9 = 2.025;
    const float WLFilter::SIGMA_11 = 2.550;

    double WLFilter::binomialCoefficient(unsigned long long l, unsigned long long k){
        if(l < k){
            PyErr_SetString(PyExc_RuntimeError, "binomial coefficient values are not correct.");
            return 0.0;
        }else if (l == k){
            return 1.0;
        }else{
            double result = 1;
            for(unsigned long long it = l; it > k ; it--){
                result = (result * it) / (it - k);
            }
            return result;
        }
    }

    bool WLFilter::_gaussianFilter(cv::Mat &image, cv::Mat &outputImage,int size){
        float sigma;
        if(size % 2 == 0 || size < 3 || size > 11){
            PyErr_SetString(PyExc_RuntimeError, "Filter size not supported.");
            return false;
        }else if (size == 3)
            sigma = SIGMA_3;
        else if (size == 5)
            sigma = SIGMA_5;
        else if (size == 7)
            sigma = SIGMA_7;
        else if (size == 9)
            sigma = SIGMA_9;
        else if (size == 11)
            sigma = SIGMA_11;
        try{
            cv::GaussianBlur(image,outputImage,cv::Size(size,size),sigma,0,cv::BORDER_DEFAULT);
        }catch(cv::Exception& e){
            PyErr_SetString(PyExc_RuntimeError, e.what());
            return false;
        }
        return true;
    }

    bool WLFilter::_binomialFilter(cv::Mat &image, cv::Mat &outputImage, int width, int height){
        //create binomial kernel with double type
        cv::Point anchor = cv::Point(-1,-1);
        double delta = 0;
        cv::Mat binomialKernel = cv::Mat(height,width,CV_64F);
        double fraction = 1 / pow(2.0,(double)(height + width - 2));
        for(int x = 0 ; x < height ; x++){
            for(int y = 0 ; y < width ; y++){
                double firstBinomialCoefficient = WLFilter::binomialCoefficient(height - 1,x);
                double secondBinomialCoefficient = WLFilter::binomialCoefficient(width - 1,y);
                binomialKernel.at<double>(x,y) = fraction * firstBinomialCoefficient * secondBinomialCoefficient;
            }
        }
        //convolve image with created kernel
        try{
            cv::filter2D(image,outputImage,CV_32F,binomialKernel,anchor,delta,cv::BORDER_DEFAULT);
        }catch(cv::Exception& e){
            PyErr_SetString(PyExc_RuntimeError, e.what());
            return false;
        }
        return true;
    }

    bool WLFilter::gaussianFilter(WLObject::WLImage *image, WLObject::WLImage *outImage, int size){
        cv::Mat inputMat;
        image->getImageMat(inputMat);
        int channels = inputMat.channels();
        if(channels == 3){
            cv::cvtColor(inputMat,inputMat,cv::COLOR_RGB2GRAY);
        }else if (channels != 1){
            PyErr_SetString(PyExc_RuntimeError, "Undefined image format.");
            return false;
        }
        cv::Mat outputMat = cv::Mat();
        bool result = WLFilter::_gaussianFilter(inputMat,outputMat,size);
        cv::convertScaleAbs(outputMat,outputMat);
        outputMat.convertTo(outputMat,CV_8U);
        if (outImage->isLoaded())
            outImage->free();
        outImage->load(outputMat);
        return result;
    }

    bool WLFilter::binomialFilter(WLObject::WLImage *image, WLObject::WLImage *outImage, int width, int height){
        cv::Mat inputMat; 
        image->getImageMat(inputMat);
        int channels = inputMat.channels();
        if(channels == 3)
            cv::cvtColor(inputMat,inputMat,cv::COLOR_RGB2GRAY);
        else if(channels != 1){
            PyErr_SetString(PyExc_RuntimeError, "Undefined image format.");
            return false;
        }
        cv::Mat outputMat = cv::Mat();
        bool result = WLFilter::_binomialFilter(inputMat,outputMat,width,height);
        cv::convertScaleAbs(outputMat,outputMat);
        outputMat.convertTo(outputMat,CV_8U);
        if (outImage->isLoaded())
            outImage->free();
        outImage->load(outputMat);
        return result;
    }
}