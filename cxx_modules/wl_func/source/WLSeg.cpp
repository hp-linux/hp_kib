#include <WLSeg.hpp>
#include <iostream>
#include <fstream>

namespace WLFunc{
    //-----------------------SUPPORTING FUNCTIONS (IN FILE)-------------------------------------
    bool WLSeg::_checkConnectedPath(const cv::Mat &image,int rows, int cols, int x, int y, int currentLength, int maxLength){
        if(currentLength > maxLength)
            return true;
        uchar top = 0, bottom = 0, left = 0, right = 0, topLeft = 0, topRight = 0, botLeft = 0, botRight = 0;
        if(x - 1 >= 0){
            top = image.at<uchar>(x - 1,y);
            if (top == 255) //recursive
                if(_checkConnectedPath(image,rows,cols,x - 1,y,currentLength + 1,maxLength))
                    return true;
        }
        if(x + 1 < rows){ 
            bottom = image.at<uchar>(x + 1,y);
            if(bottom == 255)
                if(_checkConnectedPath(image,rows,cols,x + 1,y,currentLength + 1,maxLength))
                    return true;
        }
        if(y - 1 >= 0){
            left = image.at<uchar>(x,y - 1);
            if(left == 255)
                if(_checkConnectedPath(image,rows,cols,x,y - 1,currentLength + 1,maxLength))
                    return true;
        }
        if(y + 1 < cols){
            right = image.at<uchar>(x,y + 1);
            if(right == 255)
                if(_checkConnectedPath(image,rows,cols,x,y + 1,currentLength + 1,maxLength))
                    return true;
        }
        if(x - 1 >= 0 && y - 1 >= 0){
            topLeft = image.at<uchar>(x - 1,y - 1);
            if(topLeft == 255)
                if(_checkConnectedPath(image,rows,cols,x - 1,y - 1,currentLength + 1,maxLength))
                    return true;
        }
        if(x - 1 >= 0 && y + 1 < cols){
            topRight = image.at<uchar>(x - 1,y + 1);
            if(topRight == 255)
                if(_checkConnectedPath(image,rows,cols,x - 1,y + 1,currentLength + 1,maxLength))
                    return true;
        }
        if(x + 1 < rows && y - 1 >= 0){
            botLeft = image.at<uchar>(x + 1,y - 1);
            if(botLeft == 255)
                if(_checkConnectedPath(image,rows,cols,x + 1,y - 1,currentLength + 1,maxLength))
                    return true;
        }
        if(x + 1 < rows && y + 1 < cols){
            botRight = image.at<uchar>(x + 1,y + 1);
            if(botRight == 255)
                if(_checkConnectedPath(image,rows,cols,x + 1,y + 1,currentLength + 1,maxLength))
                    return true;
        }
        return false;
    }

    bool WLSeg::_divideRegion(cv::Mat &inputRegion,cv::Mat &outputRegion, int x, int y, int connectivity){
        int rows = inputRegion.rows;
        int cols = inputRegion.cols;
        if(x < 0 || x >= cols || y < 0 || y >= rows){
            return false;
        }
        if(inputRegion.at<uchar>(x,y) == 0){
            return false;
        }else{
            outputRegion.at<uchar>(x,y) = inputRegion.at<uchar>(x,y);
            inputRegion.at<uchar>(x,y) = 0;
            //left right top bottom
            _divideRegion(inputRegion,outputRegion,x - 1,y,connectivity);
            _divideRegion(inputRegion,outputRegion,x,y + 1,connectivity);
            _divideRegion(inputRegion,outputRegion,x,y - 1,connectivity);
            _divideRegion(inputRegion,outputRegion,x,y + 1,connectivity);
            if(connectivity == 8){ //extra connectivity
                _divideRegion(inputRegion,outputRegion,x - 1,y - 1,connectivity);
                _divideRegion(inputRegion,outputRegion,x - 1,y + 1,connectivity);
                _divideRegion(inputRegion,outputRegion,x + 1,y - 1,connectivity);
                _divideRegion(inputRegion,outputRegion,x + 1,y + 1,connectivity);
            }
            return true;
        }
    }
    
    bool WLSeg::hysteresisThreshold(WLObject::WLImage * const inputImage, WLObject::WLRegion *outputRegion,
    int low, int high, int maxLength){
        //region must be empty
        if(low > high){
            PyErr_SetString(PyExc_RuntimeError, "low cannot be larger than high.");
            return false;
        }
        cv::Mat inputMat;
        inputImage->getImageMat(inputMat);
        int rows = inputImage->getHeight(); //numer of row
        int cols = inputImage->getWidth(); //number of column
        if(inputImage->getType() == WLObject::WLImage::RGB){
            cv::cvtColor(inputMat,inputMat,cv::COLOR_RGB2GRAY);
            inputMat.convertTo(inputMat,CV_8U);
        }

        //double threshold
        for(int x = 0 ; x < rows ; x++){
            for(int y = 0 ; y < cols ; y++){
                if (inputMat.at<uchar>(x,y) > high){
                    inputMat.at<uchar>(x,y) = 255;
                }
                else if(inputMat.at<uchar>(x,y) >= low)
                    inputMat.at<uchar>(x,y) = low;
                else
                    inputMat.at<uchar>(x,y) = 0;
            }
        }

        //hysteresis threshold + detect region
        //std::vector<cv::Point> *pointList = new std::vector<cv::Point>();
        for(int x = 0 ; x < rows ; x++){
            for(int y = 0 ; y < cols ; y++){
                if(inputMat.at<uchar>(x,y) == low){ //weak pixel
                    if (WLSeg::_checkConnectedPath(inputMat,rows,cols,x - 1,y - 1,1,maxLength) ||
                        WLSeg::_checkConnectedPath(inputMat,rows,cols,x,y - 1,1,maxLength) ||
                        WLSeg::_checkConnectedPath(inputMat,rows,cols,x + 1,y - 1,1,maxLength) ||
                        WLSeg::_checkConnectedPath(inputMat,rows,cols,x - 1,y,1,maxLength) ||
                        WLSeg::_checkConnectedPath(inputMat,rows,cols,x + 1,y,1,maxLength) ||
                        WLSeg::_checkConnectedPath(inputMat,rows,cols,x - 1,y + 1,1,maxLength) ||
                        WLSeg::_checkConnectedPath(inputMat,rows,cols,x,y + 1,1,maxLength) ||
                        WLSeg::_checkConnectedPath(inputMat,rows,cols,x + 1,y + 1,1,maxLength)){
                        inputMat.at<uchar>(x,y) = 255;
                    }else
                        inputMat.at<uchar>(x,y) = 0;
                }
            }
        }
        //update region
        outputRegion->addRegion(inputMat);
        return true;
    }

    bool WLSeg::thresholdWithBoundary(WLObject::WLImage *inputImage, WLObject::WLRegion *outputRegion, int thresholdVal, int maxThresholdVal){
        cv::Mat binarizedMat;
        inputImage->getImageMat(binarizedMat);
        //convert image to grayscale
        if(inputImage->getType() == WLObject::WLImage::RGB){
            cv::cvtColor(binarizedMat,binarizedMat,cv::COLOR_RGB2GRAY);
            binarizedMat.convertTo(binarizedMat,CV_8U);
        }

        //begin binarizing
        try{
            cv::threshold(binarizedMat,binarizedMat,thresholdVal,maxThresholdVal,cv::THRESH_BINARY);
        }catch(cv::Exception &e){
            PyErr_SetString(PyExc_RuntimeError,"What is the error?");
            return false;
        }

        outputRegion->addRegion(binarizedMat);
        return true;
    }

    void print(std::string d){
        PyErr_WarnEx(PyExc_Warning,d.c_str(),d.size() * sizeof(char));
    }

    bool WLSeg::threshold(WLObject::WLImage *inputImage, WLObject::WLRegion *outputRegion, int thresholdVal){
        return thresholdWithBoundary(inputImage,outputRegion,thresholdVal,255);
    }

    bool WLSeg::divideConnectedRegion(WLObject::WLRegion *inputRegion,WLObject::WLRegion *outputRegion, int idx,int connectivity){
        std::vector<cv::Mat> regionList;
        cv::Mat region;
        if (connectivity != 4 && connectivity != 8){
            PyErr_SetString(PyExc_RuntimeError,"Number of connectivity is not valid");
            return false;
        }
        if(idx == -1){ //divide all regions in WLRegion
            int regionSize = inputRegion->size();
            for(int i = 0 ; i < regionSize ; i++){
                cv::Mat dividedRegion;
                inputRegion->getRegion(i,dividedRegion);
                int rows = dividedRegion.rows;
                int cols = dividedRegion.cols;
                for(int x = 0 ; x < rows ; x++){
                    for(int y = 0 ; y < cols ; y++){
                        region = cv::Mat(rows,cols,CV_8U,cv::Scalar(0));
                        bool success = _divideRegion(dividedRegion,region,x,y,connectivity);
                        if(success){
                            regionList.push_back(region);
                        }
                    }
                }
            }
        }else{
            int regionSize = inputRegion->size();
            if(idx < 0 || idx >= regionSize){
                PyErr_SetString(PyExc_RuntimeError,"Index out of bound.");
                return false;
            }
            for(int i = 0 ; i < idx ; i++){ //push all region before index
                cv::Mat temp;
                inputRegion->getRegion(i,temp);
                regionList.push_back(temp);
            }
            
            //divide region
            cv::Mat dividedRegion;
            inputRegion->getRegion(idx,dividedRegion);
            int rows = dividedRegion.rows;
            int cols = dividedRegion.cols;
            for(int x = 0 ; x < rows ; x++){
                for(int y = 0 ; y < cols ; y++){
                    region = cv::Mat(rows,cols,dividedRegion.type(),cv::Scalar(0));
                    bool success = _divideRegion(dividedRegion,region,x,y,connectivity);
                    if(success){
                        regionList.push_back(region);
                    }
                }
            }

            //copy remaining region
            for(int i = idx + 1 ; i < inputRegion->size() ; i++){
                cv::Mat temp;
                inputRegion->getRegion(i,temp);
                regionList.push_back(temp);
            }
        }
        //copy region to outputRegion
        outputRegion->clear();
        for(int i = 0 ; i < regionList.size() ; i++)
            outputRegion->addRegion(regionList[i]);
        return true;
    }
}