
#include <WLEdgeDetector.hpp>

namespace WLFunc{
    //------------------------------------------------------------------------------------------

    //WLEdgeDetector static constant initialization
    const std::string WLEdgeDetector::GAUSSIAN_FILTER = "gaussian_filter";
    const std::string WLEdgeDetector::BINOMIAL_FILTER = "binomial_filter";

    const std::string WLEdgeDetector::SUM_ABS = "sum_abs";
    const std::string WLEdgeDetector::SUM_SQRT = "sum_sqrt";
    const std::string WLEdgeDetector::SUM_ABS_ON_DIRECTION = "sum_abs_on_direction";
    const std::string WLEdgeDetector::MAX_ABS_ON_DIRECTION = "max_abs_on_direction";
    const std::string WLEdgeDetector::X = "x";
    const std::string WLEdgeDetector::Y = "y";
    //Input: Grayscale image
    //DESCRIPTION: Calculate the first derivative of the image (Using Sobel operator)
    bool WLEdgeDetector::sobelAmplitude(WLObject::WLImage *const inputImage, WLObject::WLImage *outputImage,
    std::string type, std::string filterType, int filterSize){
        //pointers that will not be deleted!
        cv::Mat resultMat;
        inputImage->getImageMat(resultMat);
        //convert 3 channels to 1 channel
        int channels = resultMat.channels();
        if(channels == 3){ //rgb image
            cv::cvtColor(resultMat,resultMat,cv::COLOR_RGB2GRAY);
        }else if (channels != 1){ //not grayscale image
            PyErr_SetString(PyExc_RuntimeError, "Undefined image format.");
            return false;
        }
        //convert to 32F
        resultMat.convertTo(resultMat,CV_32F);
        //0. Filter (if filter size is larger than 3)
        if(filterSize > 3){
            if(filterType == WLEdgeDetector::GAUSSIAN_FILTER){
                WLFilter::_gaussianFilter(resultMat,resultMat,filterSize);
            }else if (filterType == WLEdgeDetector::BINOMIAL_FILTER){
                WLFilter::_binomialFilter(resultMat,resultMat,filterSize,filterSize);
            }else{
                PyErr_SetString(PyExc_RuntimeError, "Filter type not available.");
                return false;
            }
        }
        //1. Begin detecting edges
        if(type != WLEdgeDetector::X && type != WLEdgeDetector::Y){
            int scale = 1;
            int delta = 0;
            cv::Mat xMat, yMat, scaledXMat, scaledYMat;

            try{
                cv::Sobel(resultMat,xMat,CV_32F,1,0,3,scale,delta,cv::BORDER_DEFAULT);
                cv::convertScaleAbs(xMat,scaledXMat);
                scaledXMat.convertTo(scaledXMat,CV_8U);
                cv::Sobel(resultMat,yMat,CV_32F,0,1,3,scale,delta,cv::BORDER_DEFAULT);
                cv::convertScaleAbs(yMat,scaledYMat);
                scaledYMat.convertTo(scaledYMat,CV_8U);
            }catch(cv::Exception& e){
                PyErr_SetString(PyExc_RuntimeError, e.what());
                return false;
            }
            if(type == SUM_SQRT){
                int rows = inputImage->getHeight();
                int cols = inputImage->getWidth();
                for(int x = 0 ; x < rows ; x++){
                    for(int y = 0 ; y < cols ; y++){
                        float gx = (float)scaledXMat.at<uchar>(x,y);
                        float gy = (float)scaledYMat.at<uchar>(x,y);
                        resultMat.at<float>(x,y) = sqrt(gx * gx + gy * gy) / 4;
                    }
                }
            }else if(type == WLEdgeDetector::SUM_ABS){
                cv::addWeighted(scaledXMat,0.25,scaledYMat,0.25,0,resultMat);
            }else if (type == WLEdgeDetector::SUM_ABS_ON_DIRECTION){
                
            }else if (type == WLEdgeDetector::MAX_ABS_ON_DIRECTION){

            }else{
                PyErr_SetString(PyExc_RuntimeError, "gradient type not available.");
                return false;
            }
        }else if(type == WLEdgeDetector::X || type == WLEdgeDetector::Y){
            int scale = 1;
            int delta = 0;
            try{
                if (type == X)
                    cv::Sobel(resultMat,resultMat,CV_32F,1,0,3,scale,delta,cv::BORDER_DEFAULT);
                else
                    cv::Sobel(resultMat,resultMat,CV_32F,0,1,3,scale,delta,cv::BORDER_DEFAULT);
            }catch(cv::Exception& e){
                PyErr_SetString(PyExc_RuntimeError, e.what());
                return false;
            }
            resultMat.convertTo(resultMat,CV_32F);
            resultMat = resultMat / 4;
        }else{
            PyErr_SetString(PyExc_RuntimeError, "x and y cannot be set to 0 at the same time.");
            return false;
        }
        cv::convertScaleAbs(resultMat,resultMat);
        resultMat.convertTo(resultMat,CV_8U);
        if (outputImage->isLoaded())
            outputImage->free();
        outputImage->load(resultMat);
        
        return true;
    }
}