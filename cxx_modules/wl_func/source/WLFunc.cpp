#include <WLFunc.hpp>

namespace WLFunc{

    BOOST_PYTHON_MODULE(WLFunc) {
        using namespace boost::python;

        //exposing classes
        class_<WLEdgeDetector>("WLEdgeDetector",no_init)
        .def_readonly("GAUSSIAN_FILTER",&WLEdgeDetector::GAUSSIAN_FILTER)
        .def_readonly("BINOMIAL_FILTER",&WLEdgeDetector::BINOMIAL_FILTER )
        .def_readonly("SUM_ABS",&WLEdgeDetector::SUM_ABS)
        .def_readonly("SUM_SQRT",&WLEdgeDetector::SUM_SQRT)
        .def_readonly("SUM_ABS_ON_DIRECTION",&WLEdgeDetector::SUM_ABS_ON_DIRECTION)
        .def_readonly("MAX_ABS_ON_DIRECTION",&WLEdgeDetector::MAX_ABS_ON_DIRECTION)
        .def_readonly("X",&WLEdgeDetector::X)
        .def_readonly("Y",&WLEdgeDetector::Y)
        .def("sobelAmplitude",&WLEdgeDetector::sobelAmplitude);

        class_<WLFilter>("WLFilter",no_init)
        .def("gaussianFilter",&WLFilter::gaussianFilter)
        .def("binomialFilter",&WLFilter::binomialFilter);

        class_<WLSeg>("WLSeg",no_init)
        .def("hysteresisThreshold",&WLSeg::hysteresisThreshold)
        .def("threshold",&WLSeg::threshold)
        .def("thresholdWithBoundary",&WLSeg::thresholdWithBoundary)
        .def("divideConnectedRegion",&WLSeg::divideConnectedRegion);
    }

} //end namespace WLObject