#include <WLRegion.hpp>
namespace WLObject{

    WLRegion::WLRegion(){
        regions = new std::vector<cv::Mat>();
        color = new std::vector<cv::Vec3b>();
    }

    WLRegion::~WLRegion(){
        delete regions, color;
    }

    void WLRegion::clear(){
        delete regions, color;
        regions = new std::vector<cv::Mat>();
        color = new std::vector<cv::Vec3b>();
    }

    void WLRegion::clearRegion(int idx){
        if(idx < 0 || idx >= regions->size()){
            PyErr_SetString(PyExc_RuntimeError,"Specified index out of bound.");
            return;
        }
        regions->erase(regions->begin() + idx - 1);
        color->erase(color->begin() + idx - 1);
        return;
    }

    int WLRegion::size(){
        return regions->size();
    }

    bool WLRegion::addRegion(cv::Mat &region){
        if(regions->size() != 0){
            int cols = (*regions)[0].cols;
            int rows = (*regions)[0].rows;
            if(region.cols != cols || region.rows != rows){
                PyErr_SetString(PyExc_RuntimeError,"Region not in same size as other region in WLRegion");
                return false;
            }   
        }
        cv::Mat tmp = region.clone();
        regions->push_back(tmp);
        //generate random color
        srand(time(0));
        cv::Vec3b c;
        bool found = true;
        while(found){
            c[0] = rand() % 256; c[1] = rand() % 256; c[2] = rand() % 256;
            found = false;
            for(int x = 0 ; x < color->size() ; x++){
                if (c[0] == (*color)[x][0] &&
                    c[1] == (*color)[x][1] &&
                    c[2] == (*color)[x][2]){
                        found = true;
                        break;
                    }
            }
        }
        color->push_back(c);
        return true;
    }

    bool WLRegion::getRegion(int idx,cv::Mat &output){
        if (idx < 0 || idx >= regions->size()){
            PyErr_SetString(PyExc_RuntimeError,"Specified index out of bound.");
            return false;
        }
        output = (*regions)[idx];
        return true;
    }

    //DESCRIPTION:
    //input: any image
    //output: RGB image !
    bool WLRegion::applyOnImage(WLImage *input, WLImage *output, int idx){
        //input image : CV_8U
        cv::Mat appliedImage;
        input->getImageMat(appliedImage);
        if(input->getType() == WLImage::GRAYSCALE)
            cv::cvtColor(appliedImage,appliedImage,cv::COLOR_GRAY2RGB);
        appliedImage.convertTo(appliedImage,CV_8UC3);
        srand(time(0));
        if(idx == -1){
            if(regions->size() == 0){
                PyErr_SetString(PyExc_RuntimeError,"No region available.");
                return false;
            }
            int rows = appliedImage.rows;
            int cols = appliedImage.cols;
            int regionRows = (*regions)[0].rows;
            int regionCols = (*regions)[0].cols;
            if(regionRows != rows || regionCols != cols){
                PyErr_SetString(PyExc_RuntimeError,"Image size is not the same as the size of region.");
                return false;
            }
            //apply region on image
            for(int x = 0 ; x < cols ; x++){
                for(int y = 0 ; y < rows ; y++){
                    int t = regions->size() - 1;
                    for(; t >= 0 ; t--){
                        if((*regions)[t].at<uchar>(x,y) == 255)
                            break;
                    }
                    if(t >= 0){
                        appliedImage.at<cv::Vec3b>(x,y) = (*color)[t];
                        
                    }
                }
            }
        }else{
            //check idx
            if(idx < 0 || idx >= regions->size()){
                PyErr_SetString(PyExc_RuntimeError,"Region index out of bound.");
                return false;
            }
            int rows = appliedImage.rows;
            int cols = appliedImage.cols;
            int regionRows = (*regions)[idx].rows;
            int regionCols = (*regions)[idx].cols;
            if(regionRows != rows || regionCols != cols){
                PyErr_SetString(PyExc_RuntimeError,"Image size is not the same as the size of region.");
                return false;
            }
            //apply region on image
            for(int x = 0 ; x < cols ; x++){
                for(int y = 0 ; y < rows ; y++){
                    if((*regions)[idx].at<uchar>(x,y) == 255){
                        PyEnsureGIL gil;
                        appliedImage.at<cv::Vec3b>(x,y) = (*color)[idx];
                    }
                }
            }
        }
        //update WLImage
        if (output->isLoaded())
            output->free();
        output->load(appliedImage);
        return true;
    }

    boost::python::list WLRegion::at(int idx, int x, int y){
        if (idx < 0 || idx >= regions->size()){
            PyErr_SetString(PyExc_RuntimeError,"Specified index out of bound.");
            return boost::python::list();
        }
        int rows = (*regions)[idx].rows;
        int cols = (*regions)[idx].cols;
        if (y < 0 || y >= rows || x < 0 || x >= cols){
            PyErr_SetString(PyExc_RuntimeError,"Specified index out of bound.");
            return boost::python::list();
        }
        return CV4Converter::cvPointToPythonList((*regions)[idx].at<cv::Point>(x,y));
    }

    bool WLRegion::createWLImage(WLImage *input, int idx){
        if (idx < 0 || idx >= regions->size()){
            PyErr_SetString(PyExc_RuntimeError,"Specified index out of bound.");
            return false;
        }
        if(input->isLoaded())
            input->free();
        input->load((*regions)[idx]);
        return true;
    }
}