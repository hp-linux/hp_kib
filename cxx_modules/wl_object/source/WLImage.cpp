#include <WLImage.hpp>
namespace WLObject{

    const int WLImage::RGB = CV_8UC3;
    const int WLImage::GRAYSCALE = CV_8U;

    WLImage::WLImage(){
            isLoad = false;
            type = WLImage::RGB;
    }

    WLImage::WLImage(std::string path, int t) : WLImage(){
        isLoad = load(path,t);
        if(!isLoad){
            PyErr_SetString(PyExc_RuntimeError,"Unable to load the image.");
        }
    }

    WLImage::~WLImage(){
        image.release();
    }

    bool WLImage::load(std::string path, int t){
        using namespace cv;
        //Begin reading image file
        try{
            image = cv::Mat(cv::imread(path,IMREAD_ANYCOLOR));
            //not understand why??
            if(t == WLImage::RGB)
                cvtColor(image,image,COLOR_BGR2RGB);
            else if(t == WLImage::GRAYSCALE){
                cvtColor(image,image,COLOR_BGR2RGB);
                cvtColor(image,image,COLOR_RGB2GRAY);
            }else{
                PyErr_SetString(PyExc_RuntimeError, "Type not defined.");
                return false;
            }
            image.convertTo(image,t);
        }catch(Exception& e){
            PyErr_SetString(PyExc_RuntimeError, e.what());
            return false;
        }
        type = t;
        return true;
    }

    bool WLImage::load(cv::Mat &mat){
        cv::Mat cpMat = mat;
        using namespace cv;
        isLoad = true;
        int channels = cpMat.channels();
        if(channels == 1){ //grayscale
            cpMat.convertTo(cpMat,WLImage::GRAYSCALE);
            type = WLImage::GRAYSCALE;
        }else if (channels == 3){ //rgb
            cpMat.convertTo(cpMat,WLImage::RGB);
            type = WLImage::RGB;
        }else{
            PyErr_SetString(PyExc_RuntimeError, "Image type not supported.");
            return false;
        }
        image = cpMat;
        return isLoad;
    }

    PyObject* WLImage::getImageBytes(){
        if(!isLoad){
            PyErr_SetString(PyExc_RuntimeError,"Image not loaded.");
            return NULL;
        }
        cv::Mat byteImage = image.clone();
        if(type == WLImage::GRAYSCALE)
            cvtColor(byteImage,byteImage,cv::COLOR_GRAY2RGB);
        return CV4Converter::convert(byteImage);
    }

    bool WLImage::getImageMat(cv::Mat &output){
        if(!isLoad){
            PyErr_SetString(PyExc_RuntimeError,"Image is not loaded.");
            return false;
        }
        output = image.clone();
        return true;
    }

    int WLImage::getType(){
        return type;
    }

    int WLImage::getHeight(){
        return image.rows;
    }

    int WLImage::getWidth(){
        return image.cols;
    }
    boost::python::list WLImage::at(int x, int y){
        //check x and y
        y = image.rows - y;
        try{
            if(type == WLImage::RGB){
                const cv::Vec3b color = image.at<cv::Vec3b>(y, x);
                return CV4Converter::vec3bToPythonList(color);
            }else if(type == WLImage::GRAYSCALE){
                uchar grayscaleIntensity = image.at<uchar>(y, x);
                const cv::Vec3b color(grayscaleIntensity,grayscaleIntensity,grayscaleIntensity);
                return CV4Converter::vec3bToPythonList(color);
            }else{
                PyErr_SetString(PyExc_RuntimeError,"Type not defined.");
                return boost::python::list();
            }
            
        }catch(...){
            //TODO: Out of bound
            return boost::python::list();
        }
    }

    void WLImage::free(){
        if(!isLoad){
            PyErr_SetString(PyExc_RuntimeError,"What is the error?");
            return;
        }
        isLoad = false;
        type = WLImage::RGB;
        image.release();
    }

    bool WLImage::isLoaded(){
        return isLoad;
    }

    bool WLImage::clone(WLImage *input){
        if(input->isLoaded())
            input->free();
        cv::Mat newImage = image.clone();
        input->load(newImage);
        return true;
    }

} //end namespace WLObject