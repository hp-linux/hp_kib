#include <CV4Converter.hpp>
namespace WLObject{
    //STANDALONE FUNCTION
    //CONVERTER INTERFACE (BETWEEN C++ AND PYTHON)
    boost::python::list CV4Converter::vec3bToPythonList(const cv::Vec3b &v){
        boost::python::list l;
        for(int i = 0 ; i < 3 ; i++)
            l.append(v[i]);
        return l;
    }
    boost::python::list CV4Converter::vec4bToPythonList(const cv::Vec4b &v){
        boost::python::list l;
        for(int i = 0 ; i < 4 ; i++)
            l.append(v[i]);
        return l;
    }

    boost::python::list CV4Converter::cvPointToPythonList(const cv::Point &p){
        boost::python::list l;
        l.append(p.x);
        l.append(p.y);
        return l;
    }

    PyObject* CV4Converter::convert(cv::Mat obj){
        //PARAMETERS
        size_t width = obj.size().width;
        size_t height = obj.size().height;
        size_t channels = obj.channels();

        size_t imageSize = width * height * channels;
        #if (PY_VERSION_HEX >= 0x03000000)
            return PyBytes_FromStringAndSize((const char*)obj.data,imageSize);
        #else
            return PyString_FromStringAndSize((const char*)obj.data,imageSize);
        #endif
    }
}