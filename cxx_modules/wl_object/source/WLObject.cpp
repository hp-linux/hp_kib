#include <WLObject.hpp>
namespace WLObject{
    
    BOOST_PYTHON_MODULE(WLObject) {

        PyEval_InitThreads();
        using namespace boost::python;
        //expose class
        class_<WLImage>("WLImage")
        .def(init<std::string,int>())
        .def<bool (WLImage::*)(std::string,int)>("load",&WLImage::load)
        .def("getImageBytes",&WLImage::getImageBytes)
        .def("getType",&WLImage::getType)
        .def("getHeight",&WLImage::getHeight)
        .def("getWidth",&WLImage::getWidth)
        .def("at",&WLImage::at)
        .def("free",&WLImage::free)
        .def("isLoaded",&WLImage::isLoaded)
        .def_readonly("RGB",&WLImage::RGB)
        .def_readonly("GRAYSCALE",&WLImage::GRAYSCALE)
        .def("clone",&WLImage::clone);

        class_<WLRegion>("WLRegion")
        .def("clear",&WLRegion::clear)
        .def("at",&WLRegion::at)
        .def("size",&WLRegion::size)
        .def("applyOnImage",&WLRegion::applyOnImage)
        .def("createWLImage",&WLRegion::createWLImage);
    }

} //end namespace WLObject