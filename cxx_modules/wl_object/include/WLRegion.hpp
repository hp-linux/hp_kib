#pragma once

#include <opencv4/opencv2/highgui.hpp>
#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/imgcodecs.hpp>
#include <CV4Converter.hpp>
#include <GILEnsure.hpp>
#include <vector>
#include <WLImage.hpp>
#include <iostream>
#include <ctime>    // For time()
#include <cstdlib>  // For srand() and rand()
namespace WLObject{
    class WLRegion{
        private:
            std::vector<cv::Mat> *regions;
            std::vector<cv::Vec3b> *color;
        public:
            WLRegion();
            ~WLRegion();
            void clear();
            void clearRegion(int idx);
            int size();
            bool addRegion(cv::Mat &region);
            bool addRegion(std::vector<cv::Point> region, int width, int height);
            bool getRegion(int idx,cv::Mat &output);
            std::vector<cv::Point> getRegionInVector(int idx);
            bool applyOnImage(WLImage *input, WLImage *output, int idx);
            bool createWLImage(WLImage *input, int idx);
            boost::python::list at(int idx,int x, int y);
    };
}