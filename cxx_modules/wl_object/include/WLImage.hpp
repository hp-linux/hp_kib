#pragma once

#include <opencv4/opencv2/highgui.hpp>
#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/imgcodecs.hpp>
#include <CV4Converter.hpp>
namespace WLObject{
    class WLImage{
        private:
            cv::Mat image;
            int type;
            bool isLoad;
        public:
            static const int RGB;
            static const int GRAYSCALE;
            WLImage();
            WLImage(std::string path, int t);
            ~WLImage();
            bool load(std::string path, int t);
            bool load(cv::Mat& mat);
            PyObject* getImageBytes();
            bool getImageMat(cv::Mat &output);
            int getType();
            int getHeight();
            int getWidth();
            boost::python::list at(int x, int y);
            void free();
            bool isLoaded();
            bool clone(WLImage *input);
    };
}