#pragma once

#include <boost/python/object.hpp>
#include <boost/python/list.hpp>
#include <boost/python.hpp>
#include <opencv4/opencv2/core.hpp>
#include <opencv4/opencv2/core/matx.hpp>
#include <string>
#include <Python.h>
#include <vector>
namespace WLObject{
    //Standalone Functions
    //#############################STRUCT CONVERTER########################
    struct CV4Converter{
        static boost::python::list vec3bToPythonList(const cv::Vec3b& v);
        static boost::python::list vec4bToPythonList(const cv::Vec4b& v);
        static boost::python::list cvPointToPythonList(const cv::Point &p);
        static PyObject* convert(cv::Mat obj);
    };
}