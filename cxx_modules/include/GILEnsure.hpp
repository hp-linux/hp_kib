#include <Python.h>

class PyAllowThreads
    {
    public:
        PyAllowThreads() : _state(PyEval_SaveThread()) {}
        ~PyAllowThreads()
        {
            PyEval_RestoreThread(_state);
        }
    private:
        PyThreadState* _state;
    };

    class PyEnsureGIL
    {
    public:
        PyEnsureGIL() : _state(PyGILState_Ensure()) {}
        ~PyEnsureGIL()
        {
            PyGILState_Release(_state);
        }
    private:
        PyGILState_STATE _state;
    };