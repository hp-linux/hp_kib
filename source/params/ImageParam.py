from source.params.SysConf import objectName, sysName

class WLImageParams:
    #Zoom value: max and min value
    ZOOM_MAX_VALUE = 32
    ZOOM_MIN_VALUE = 1

    BINARIZE_ACTION = 1
    def __init__(self,parent,key,\
        viewFrame=None,scrollArea=None,label=None,path=None):
        if key not in objectName:
            raise Exception("Wrong key")
        self.imageOn = False
        self.image = None
        self.imageHolder = None
        self.viewFrame = viewFrame
        self.scrollArea = scrollArea
        self.label = label
        self.binarizeOn = False
        self.binarizeValue = 0
        self.zoomValue = 1
        self.syncViewOn = False
        self.dragModeOn = False
        
    def clearParams(self):
        self.imageOn = False
        self.imageHolder = None
        self.openglViewer = None
        self.binarizeOn = False
        self.binarizeValue = 0
        self.zoomValue = 1
        self.syncViewOn = False
        self.dragModeOn = False