objectName = [
    "bottom_bf",
    "bottom_ring",
    "top_upper",
    "bottom_df",
    "bottom_power"
]

sysName = {
    objectName[0] : "Bottom BF",
    objectName[1] : "Bottom Ring",
    objectName[2] : "Top Upper",
    objectName[3] : "Bottom DF",
    objectName[4] : "Bottom Power"
}