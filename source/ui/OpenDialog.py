
from forms.menu_objects.menu_file_objects.forms_py.OpenDialog import Ui_WLOpenDialog
from forms.miscellanous_objects.MessageBox import MessageBox

from PyQt5 import QtCore, QtWidgets, Qt

from source.params.SysConf import objectName

import os.path, getpass, glob

from cxx_modules.WLObject import WLImage

class WLOpenDialog(QtWidgets.QDialog):
    #Constant expression for later use
    FROM_PROJECT = 1
    FROM_IMAGE = 2
    EXTENSION = ['.jpg','.bmp','.png']

    #DESCRIPTION: Initialize method for class
    def __init__(self,parent=None,typeDialog=FROM_PROJECT,defaultDir=None):
        super(WLOpenDialog,self).__init__(parent=parent)
        #Setting up UI
        self.parent = parent
        self.ui = Ui_WLOpenDialog()
        self.ui.setupUi(self)

        #Create tree mode
        if defaultDir is None:
            self.defaultDir = '/home/' + getpass.getuser()
        else:
            self.defaultDir = defaultDir
        self.type = typeDialog
        self.model = QtWidgets.QFileSystemModel()
        self.model.setRootPath('/')
        self.ui.tree.setModel(self.model)
        self.ui.tree.setRootIndex(self.model.index(self.defaultDir))

        #Initialize message box
        self.notExistDialog = MessageBox(QtWidgets.QMessageBox.Warning,
        title='Path not exist',
        parent=self,
        mainMessage='This path does not exist! Please choose a different path.')
        self.incorrectFormatDialog = MessageBox(QtWidgets.QMessageBox.Critical,
        title='Incorrect image format',
        parent=self,
        mainMessage='This file seems to be incorrect format. Please re-check your image and try again.')
        self.fileNotFolderDialog = MessageBox(QtWidgets.QMessageBox.Critical,
        title='Please choose file',
        parent=self,
        mainMessage='Please choose your image.')
        self.folderNotFileDialog = MessageBox(QtWidgets.QMessageBox.Critical,
        title='Please choose folder',
        parent=self,
        mainMessage='Please choose your image folder')

        #Events and signals
        self.ui.buttonBox.accepted.disconnect()
        self.ui.buttonBox.rejected.disconnect()
        self.ui.tree.clicked.connect(self.saveToLineEdit)
        if self.type == self.FROM_PROJECT:
            self.ui.tree.doubleClicked.connect(self.openDirectory)
            self.ui.buttonBox.accepted.connect(self.openDirectoryFromButton)
        elif self.type == self.FROM_IMAGE:
            self.ui.tree.doubleClicked.connect(self.openFile)
            self.ui.buttonBox.accepted.connect(self.openFileFromButton)
            self.setWindowTitle('Load image')
        self.ui.buttonBox.rejected.connect(self.close)

    #DESCRIPTION: Get path based on index
    def getPath(self,index):
        folderIndex = self.ui.tree.selectedIndexes()[0]
        folderPath = folderIndex.model().filePath(index)
        return folderPath

    #DESCRIPTION: Open directory (project) based on path
    def _openDirectory(self,path):
        isFile = os.path.isfile(path)
        isDir = os.path.isdir(path)
        #Check if path is valid
        if isFile: #file -> not valid
            self.folderNotFileDialog.exec_()
            return False
        if isDir: #dir -> may valid
            if path[-1] == '/': #containing slash at the end
                path = path[:-1]
            fileList = glob.glob(path + '/*')
            #signals for images
            imageDict = dict()
            for name in objectName:
                imageDict[name] = False
            for filePath in fileList:
                imageFullName = filePath[filePath.rfind('/') + 1 : ]
                imageName = imageFullName[ : imageFullName.rfind('.')]
                if True in [imageFullName.endswith(ext) for ext in self.EXTENSION] \
                    and imageName in imageDict.keys() and not imageDict[imageName]:
                    imageDict[imageName] = True
                    #load image
                    self.parent.imageParam[imageName].clearParams()
                    self.parent.imageParam[imageName].image = WLImage(filePath)
                    self.parent.imageParam[imageName].imageOn = True
                    self.parent.imageChanged = True
            return True
        if not isFile and not isDir : #Directory not exist
            self.notExistDialog.exec_() 
            return False

    #DESCRIPTION: Open file based on path
    def _openFile(self,path):
        isFile = os.path.isfile(path)
        isDir = os.path.isdir(path)
        if isDir:
            self.fileNotFolderDialog.exec_()
            return False
        if isFile:
            #Check file type
            if True in [path.endswith(ext) for ext in self.EXTENSION]:
                self.parent.imageParam[self.parent.targetImage].clearParams()
                self.parent.imageParam[self.parent.targetImage].image = WLImage(path,WLImage.RGB)
                self.parent.imageParam[self.parent.targetImage].imageOn = True
                self.parent.imageChanged = True
                return True
            else:
                self.incorrectFormatDialog.exec_()
                return False
        if not isFile and not isDir : #Directory not exist
            self.notExistDialog.exec_() 
            return False
        
    #DESCRIPTION: update line edit when single clicked
    def saveToLineEdit(self,index):
        path = self.getPath(index)
        self.ui.lineEdit.setText(path)

    #DESCRIPTION: Handler for open directory event
    def openDirectory(self,index):
        path = self.getPath(index)
        result = self._openDirectory(path)
        #if true -> return to parent widget
        if result == True:
            self.close()

    #DESCRIPTION: Handler for open directory from button
    def openDirectoryFromButton(self):
        path = self.ui.lineEdit.text()
        result = self._openDirectory(path)
        if result == True:
            self.close()

    #DESCRIPTION: Handler for open file event
    def openFile(self,index):
        path = self.getPath(index)
        result = self._openFile(path)
        if result == True:
            self.close()

    #DESCRIPTION: Handler for open file from button
    def openFileFromButton(self):
        path = self.ui.lineEdit.text()
        result = self._openFile(path)
        if result == True:
            self.close()
