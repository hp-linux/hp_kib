from OpenGL.GL import *
from OpenGL.GLU import *
from PIL import Image
from PyQt5.QtOpenGL import QGLWidget
from source.params.SysConf import objectName
import math

class WLImageViewer(QGLWidget):
    def __init__(self,parent,key,label,setOriginalSize = False):
        self.parent = parent
        self.key = key
        self.value = 1
        self.width = None
        self.height = None
        self.ID = None
        self.wlImage = None
        self.setOriginalSize = setOriginalSize
        self.label = label
        #get corresponding label
        QGLWidget.__init__(self,parent)
        self.setMouseTracking(True)
        self.setAcceptDrops(True)

    def initializeGL(self):
        glEnable(GL_TEXTURE_2D)
        glEnable(GL_DEPTH_TEST)
        self.imageSetup()
        scale = self.width / self.height
        widgetSize = self.size()
        if self.setOriginalSize:
            self.setFixedSize(self.width,self.height)
        else:
            self.setFixedHeight(widgetSize.width() / scale)
    
    def paintGL(self):
        glClearColor(0.12,0.15,0.13,1.0)
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
        glLoadIdentity()
        if self.wlImage is not None:
            glBegin(GL_QUADS)
            glTexCoord2f(0.0,0.0)
            glVertex3f(-1.0,-1.0,0.0)
            glTexCoord2f(1.0,0.0)
            glVertex3f(1.0,-1.0,0.0)
            glTexCoord2f(1.0,1.0)
            glVertex3f(1.0,1.0,0.0)
            glTexCoord2f(0.0,1.0)
            glVertex3f(-1.0,1.0,0.0)
            glEnd()
            glFlush()

    def imageSetup(self):
        if self.ID is not None:
            glDeleteTextures([self.ID])
        self.ID = glGenTextures(1)
        self.wlImage = self.parent.imageParam[self.key].image
        self.height = self.wlImage.getHeight()
        self.width = self.wlImage.getWidth()
        glBindTexture(GL_TEXTURE_2D,self.ID)
        glPixelStorei(GL_UNPACK_ALIGNMENT,1)
        glTexImage2D(GL_TEXTURE_2D,0,3,self.width,self.height,0,GL_RGB,GL_UNSIGNED_BYTE, self.wlImage.getImageBytes())
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL)

    def resizeGL(self, w, h):
        glOrtho(-1.0,1.0,-1.0,1.0,-1.0,1.0)
        glViewport(0, 0, w, h)
        scale = self.width / self.height
        widgetSize = self.size()
        self.setFixedHeight(widgetSize.width() / scale)

    def mouseMoveEvent(self,event):
        viewSize = self.size()
        x = event.pos().x()
        y = event.pos().y()
        pixelWidth = math.floor(x * self.width / viewSize.width())
        pixelHeight = math.floor(y * self.height / viewSize.height())
        xCoord = int(pixelWidth)
        yCoord = int(pixelHeight)
        rgbColor = self.wlImage.at(xCoord,yCoord)
        if len(rgbColor) != 0:
            self.label.setText('Size: ' + str(self.width) + 'x' + str(self.height) + '. Pixel: (' + str(int(pixelWidth)) + ', ' + str(int(pixelHeight)) \
            + "). RGB: (" + str(rgbColor[0]) + ',' + str(rgbColor[1]) + ',' + str(rgbColor[2]) + ')' )

    def leaveEvent(self,event):
        self.label.setText('')

    #actions
    def updateImage(self):
        self.imageSetup()
        self.update()

    def zoomImage(self,beginValue,endValue):
        value = endValue / beginValue
        self.setFixedSize(self.size().width() * value,self.size().height() * value)
        self.update()

    def imageWidth(self):
        return self.width

    def imageHeight(self):
        return self.height