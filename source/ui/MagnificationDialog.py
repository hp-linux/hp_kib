from forms.main.forms_py.MagnificationDialog import Ui_WLMagnificationDialog
from PyQt5 import QtCore, QtWidgets, QtGui
from source.ui.ImageViewer import WLImageViewer
import math

class WLMagnificationDialog(QtWidgets.QDialog):
    #Constant expression for later use
    DELTA_DIV = 2
    MOUSE_DEGREE_THRESHOLD = 20

    #DESCRIPTION: initialize method
    def __init__(self,parent,key):
        super(WLMagnificationDialog,self).__init__(parent=parent)
        self.parent = parent
        self.key = key
        self.ui = Ui_WLMagnificationDialog()
        self.ui.setupUi(self)
        self.ui.backButton.clicked.connect(self.close)
        self.ui.zoomBarInspect.valueChanged.connect(lambda: self.zoomImage())
        self.viewWidget = None
        self.isInDragMode = False
        self.lastDragPosition = None
        self.prevZoomValue = 1
        self.parentBinarizeSlider = self.parent.mainUi.binarizeSlider
        self.parentBinarizeCheckBox = self.parent.mainUi.binarizeCheckBox
        self.ui.binarizeCheckBox.setChecked(self.parentBinarizeCheckBox.isChecked())
        self.ui.binarizeSlider.setEnabled(self.ui.binarizeCheckBox.isChecked())
        value = self.parentBinarizeSlider.sliderPosition()
        self.ui.binarizeSlider.setValue(value)
        self.ui.binarizeValue.setText(str(value).zfill(3))
        self.ui.binarizeCheckBox.stateChanged.connect(self.setupBinarizeAction)
        self.ui.binarizeSlider.valueChanged.connect(self.binarizeImage)
        #Set shortcut
        self.shortcutZoomUp = QtWidgets.QShortcut(QtGui.QKeySequence("Ctrl+="),self)
        self.shortcutZoomDown = QtWidgets.QShortcut(QtGui.QKeySequence("Ctrl+-"),self)
        self.shortcutBinarize = QtWidgets.QShortcut(QtGui.QKeySequence("Ctrl+B"),self)
        self.shortcutZoomUp.activated.connect(lambda: self.zoomImage(True))
        self.shortcutZoomDown.activated.connect(lambda: self.zoomImage(False))
        self.shortcutBinarize.activated.connect(lambda: self.ui.binarizeCheckBox.setChecked(not self.ui.binarizeCheckBox.isChecked()))
        #hide bar and value
        if self.parentBinarizeCheckBox.isChecked():
            self.ui.binarizeSlider.show()
            self.ui.binarizeValue.show()
        else:
            self.ui.binarizeSlider.hide()
            self.ui.binarizeValue.hide()

    #OVERRIDED METHODS
    #DESCRIPTION:
    #[showFullScreen] show widget at full screen, and load image with original size
    def showFullScreen(self):
        super().showFullScreen()
        self.viewWidget = WLImageViewer(self.parent,self.key,self.ui.infoLabel,True)
        self.ui.viewArea.setWidget(self.viewWidget)
        self.ui.viewArea.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        self.viewWidget.installEventFilter(self)
    ################################################################################33

    #EVENT AND SIGNAL CALLBACKS
    #DESCRIPTION:
    #[zoomImage] zoom current image
    def zoomImage(self, fromKeyUp=None):
        if fromKeyUp is not None:
            if fromKeyUp == True:
                self.ui.zoomBarInspect.setValue(self.ui.zoomBarInspect.value() + 1)
            else:
                self.ui.zoomBarInspect.setValue(self.ui.zoomBarInspect.value() - 1)
        value = self.ui.zoomBarInspect.value()
        self.ui.zoomValue.setText(str(value) + 'x')
        self.viewWidget.makeCurrent()
        self.viewWidget.zoomImage(self.prevZoomValue,value)
        self.prevZoomValue = value

    def setupBinarizeAction(self):
        self.ui.binarizeSlider.setEnabled(self.ui.binarizeCheckBox.isChecked())
        self.parentBinarizeCheckBox.setChecked(self.ui.binarizeCheckBox.isChecked())
        if self.ui.binarizeCheckBox.isChecked():
            self.ui.binarizeSlider.show()
            self.ui.binarizeValue.show()
        else:
            self.ui.binarizeSlider.hide()
            self.ui.binarizeValue.hide()
        self.viewWidget.makeCurrent()
        self.viewWidget.updateImage()

    def binarizeImage(self):
        binarizeThreshold = self.ui.binarizeSlider.sliderPosition()
        tmp = binarizeThreshold
        self.ui.binarizeValue.setText(str(tmp).zfill(3))
        self.parentBinarizeSlider.setValue(binarizeThreshold)
        self.parent.centralAction()
        self.viewWidget.makeCurrent()
        self.viewWidget.updateImage()

    def eventFilter(self, source, event):
        #drag scroll for scroll area + block signal from main scroll
        if event.type() == QtCore.QEvent.MouseButtonPress:
            if event.button() == QtCore.Qt.LeftButton:
                if source is self.viewWidget:
                    self.isInDragMode = True
                    self.lastDragPosition = event.pos()
                    return True
                return False
        if event.type() == QtCore.QEvent.MouseMove:
            if source is self.viewWidget:
                #update label
                viewSize = self.viewWidget.size()
                x = event.pos().x()
                y = event.pos().y()
                pixelWidth = math.floor(x * self.viewWidget.imageWidth() / viewSize.width())
                pixelHeight = math.floor(y * self.viewWidget.imageHeight() / viewSize.height())
                self.ui.infoLabel.setText('Size: ' + str(self.viewWidget.imageWidth()) + 'x' + str(self.viewWidget.imageHeight()) \
                    + '; Pixel: ' + str(int(pixelWidth)) + ', ' + str(int(pixelHeight)))
                if self.isInDragMode:
                    delta = (event.pos() - self.lastDragPosition) / self.DELTA_DIV
                    self.ui.viewArea.horizontalScrollBar().setValue(self.ui.viewArea.horizontalScrollBar().value() - delta.x())
                    self.ui.viewArea.verticalScrollBar().setValue(self.ui.viewArea.verticalScrollBar().value() - delta.y())
                    #self.lastDragPosition = event.pos()
                return True
            return False
        if event.type() == QtCore.QEvent.MouseButtonRelease:
            if event.button() == QtCore.Qt.LeftButton:
                if source is self.viewWidget:
                    self.isInDragMode = False
                    return True
                return False
        if event.type() == QtCore.QEvent.Leave:
            if source is self.viewWidget:
                self.isInDragMode = False
                self.ui.infoLabel.setText('')
                return True
            return False

        #zooming from mouse
        if event.type() == QtCore.QEvent.Wheel:
            if abs(event.angleDelta().y()) > self.MOUSE_DEGREE_THRESHOLD:
                if event.angleDelta().y() > 0: #zoom in
                    self.zoomImage(True)
                if event.angleDelta().y() < 0: #zoom out
                    self.zoomImage(False)
                return True
            return False
        return super().eventFilter(source,event)