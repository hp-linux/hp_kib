from forms.main.forms_py.MainWindow import Ui_WLMainWindow
from forms.main.forms_py.MainWidget import Ui_WLMainWidget

from source.ui.OpenDialog import WLOpenDialog
from source.ui.MagnificationDialog import WLMagnificationDialog
from source.params.SysConf import objectName, sysName
from source.params.ImageParam import WLImageParams
from source.ui.ImageViewer import WLImageViewer

from PyQt5 import QtCore, QtWidgets, QtGui, Qt, QtOpenGL

class WLMainWindow(QtWidgets.QMainWindow):
    #Constant expression for later use
    DUMP_RATIO = 1.05
    MOUSE_DEGREE_THRESHOLD = 20
    DELTA_DIV = 1.5
    BOTTOM_BF_IMAGE = objectName[0]
    BOTTOM_RING_IMAGE = objectName[1]
    TOP_UPPER_IMAGE = objectName[2]
    BOTTOM_DF_IMAGE = objectName[3]
    BOTTOM_POWER_IMAGE = objectName[4]

    #DESCRIPTION: Initialize method for class
    def __init__(self,parent=None):
        super(WLMainWindow,self).__init__(parent=parent)
        self.parent = parent
        #Setting up UI
        self.ui = Ui_WLMainWindow()
        self.ui.setupUi(self)
        self.mainUi = Ui_WLMainWidget()
        self.loadMainScreen()

        #reset configuration
        self.defaultSetting()

        #Initialize related dialogs for Wetlens
        self.openProjectDialog = WLOpenDialog(parent=self) 
        self.openFileDialog = WLOpenDialog(parent=self,typeDialog = WLOpenDialog.FROM_IMAGE)
        self.magnificationDialog = None

        #Setting up signals and events
        for key in objectName:
            viewFrame = self.imageParam[key].viewFrame
            viewFrame.setAttribute(QtCore.Qt.WA_AcceptTouchEvents)
            viewFrame.installEventFilter(self)
            scrollArea = self.imageParam[key].scrollArea
            scrollArea.viewport().installEventFilter(self)
            scrollArea.horizontalScrollBar().valueChanged.connect(lambda: self.syncView(False))
            scrollArea.verticalScrollBar().valueChanged.connect(lambda: self.syncView(False))
        self.mainUi.magnificationButton.clicked.connect(self.openMagnificationMode)
        self.mainUi.zoomInButton.clicked.connect(lambda: self.zoomImage(True))
        self.mainUi.zoomOutButton.clicked.connect(lambda: self.zoomImage(False))
        self.mainUi.syncViewCheckBox.clicked.connect(self.setupSyncView)
        self.mainUi.binarizeCheckBox.stateChanged.connect(self.setupBinarizeAction)
        self.mainUi.binarizeSlider.valueChanged.connect(lambda: self.centralAction())
        self.ui.openProjectAction.triggered.connect(self.loadProject)
        self.ui.openImageAction.triggered.connect(self.loadImage)
        self.ui.clearScreenAction.triggered.connect(self.clearScreen)
        self.ui.exitAction.triggered.connect(self.close)
        self.stateStack = QtWidgets.QUndoStack(self)
        self.ui.actionUndo.triggered.connect(self.stateStack.undo)
        self.ui.actionRedo.triggered.connect(self.stateStack.redo)
        self.ui.actionBinarize.triggered.connect(lambda: self.mainUi.binarizeCheckBox.animateClick())
        self.ui.actionOpenMagnificationMode.triggered.connect(lambda: self.mainUi.magnificationButton.animateClick())
        self.ui.actionZoomIn.triggered.connect(lambda: self.mainUi.zoomInButton.animateClick())
        self.ui.actionZoomOut.triggered.connect(lambda: self.mainUi.zoomOutButton.animateClick())
        self.ui.actionSyncView.triggered.connect(lambda: self.mainUi.syncViewCheckBox.animateClick())

    #DESCRIPTION: Get Scroll Area based on key
    def _getScrollArea(self,targetName=None):
        scrollArea = None
        if targetName is None:
            targetName = self.targetImage
        if targetName == self.BOTTOM_BF_IMAGE:
            scrollArea = self.mainUi.bottomBFScrollArea
        if targetName == self.BOTTOM_RING_IMAGE:
            scrollArea = self.mainUi.bottomRingScrollArea
        if targetName == self.TOP_UPPER_IMAGE:
            scrollArea = self.mainUi.topUpperScrollArea
        if targetName == self.BOTTOM_DF_IMAGE:
            scrollArea = self.mainUi.bottomDFScrollArea
        if targetName == self.BOTTOM_POWER_IMAGE:
            scrollArea = self.mainUi.bottomPowerScrollArea
        return scrollArea

    #DESCRIPTION: Get View Frame based on key
    def _getViewFrame(self,targetName=None):
        viewFrame = None
        if targetName is None:
            targetName = self.targetImage
        if targetName == self.BOTTOM_BF_IMAGE:
            viewFrame = self.mainUi.bottomBFViewFrame
        if targetName == self.BOTTOM_RING_IMAGE:
            viewFrame = self.mainUi.bottomRingViewFrame
        if targetName == self.TOP_UPPER_IMAGE:
            viewFrame = self.mainUi.topUpperViewFrame
        if targetName == self.BOTTOM_DF_IMAGE:
            viewFrame = self.mainUi.bottomDFViewFrame
        if targetName == self.BOTTOM_POWER_IMAGE:
            viewFrame = self.mainUi.bottomPowerViewFrame
        return viewFrame

    #DESCRIPTION: Get Label based on key
    def _getLabel(self,targetName=None):
        label = None
        if targetName is None:
            label = self.targetImage
        if targetName == self.BOTTOM_BF_IMAGE:
            label = self.mainUi.infoBottomBF
        if targetName == self.BOTTOM_RING_IMAGE:
            label = self.mainUi.infoBottomRing
        if targetName == self.TOP_UPPER_IMAGE:
            label = self.mainUi.infoTopUpper
        if targetName == self.BOTTOM_DF_IMAGE:
            label = self.mainUi.infoBottomDF
        if targetName == self.BOTTOM_POWER_IMAGE:
            label = self.mainUi.infoBottomPower
        return label

    #DESCRIPTION: Reset all configurations of Wetlen
    def defaultSetting(self):
        self.imageChanged = False
        self.imageParam = dict()
        for key in objectName:
            self.imageParam[key] = WLImageParams(self,key,\
                self._getViewFrame(key),self._getScrollArea(key),self._getLabel(key))
        self.targetImage = None #chosen target
        self.lastDragPosition = None
        self.magnificationDialog = None
    
    #DESCRIPTION: Load Main Screen Widget for Main Window
    def loadMainScreen(self):
        self.ui.mainWidget = QtWidgets.QWidget(self)
        self.mainUi.setupUi(self.ui.mainWidget)
        self.setCentralWidget(self.ui.mainWidget)

    #DESCRIPTION: load new folder (which is a project, containing 5 images)
    def loadProject(self):
        #1. Open project dialog -> read project
        #2. Initialize image holder (WLImageViewer)
        self.openProjectDialog.exec()
        if self.imageChanged:
            self.imageChanged = False
            for key in objectName:
                label = self.imageParam[key].label
                self.imageParam[key].imageHolder = WLImageViewer(self,key,label)
                scrollArea = self.imageParam[key].scrollArea
                scrollArea.setWidget(self.imageParam[key].imageHolder)
                scrollArea.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
                scrollArea.setFixedHeight(self.imageParam[key].imageHolder.size().height() * self.DUMP_RATIO)
            self.updateStatus(self.targetImage)
            #Since image is loaded -> clear screen function is enabled
            self.ui.clearScreenAction.setEnabled(True)

    #DESCRIPTION: load single image, with current target image.
    def loadImage(self):
        #1. Open project dialog -> read image
        self.openFileDialog.exec()
        if self.imageChanged:
            self.imageChanged = False
            label = self.imageParam[self.targetImage].label
            self.imageParam[self.targetImage].imageHolder = WLImageViewer(self,self.targetImage,label)
            scrollArea = self.imageParam[self.targetImage].scrollArea
            scrollArea.setWidget(self.imageParam[self.targetImage].imageHolder)
            scrollArea.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
            scrollArea.setFixedHeight(self.imageParam[self.targetImage].imageHolder.size().height() * self.DUMP_RATIO)
            self.updateStatus(self.targetImage)
            #Since image is loaded -> clear screen function is enabled
            self.ui.clearScreenAction.setEnabled(True)
 
    #DESCRIPTION: Clear screen and reset all configuration of Wetlens
    def clearScreen(self):
        self.defaultSetting()
        self.updateStatus(self.targetImage)
        self.ui.clearScreenAction.setEnabled(False)
        self.ui.actionZoomIn.setEnabled(False)
        self.ui.actionZoomOut.setEnabled(False)
        self.ui.actionBinarize.setEnabled(False)
        self.ui.actionSyncView.setEnabled(False)
        self.ui.actionOpenMagnificationMode.setEnabled(False)
        self.ui.actionUndo.setEnabled(False)
        self.ui.actionRedo.setEnabled(False)
        #reset openGLWidget\
        for key in objectName:
            scrollArea = self.imageParam[key].scrollArea
            scrollArea.setWidget(QtWidgets.QWidget(None))

    #DESCRIPTION: Zoom image based on current target image or given key value of image
    def zoomImage(self,isIn,targetImage=None):
        currentTarget = self.targetImage if targetImage is None else targetImage
        if self.imageParam[currentTarget].imageOn:
            if isIn: #zoom in
                if self.imageParam[currentTarget].zoomValue != WLImageParams.ZOOM_MAX_VALUE:
                    self.imageParam[currentTarget].zoomValue += 1
                    value = self.imageParam[currentTarget].zoomValue
                    self.mainUi.zoomValue.setText("(x" + str(value) + ")")
                    self.imageParam[currentTarget].imageHolder.makeCurrent()
                    self.imageParam[currentTarget].imageHolder.zoomImage(value - 1,value)
                    if self.imageParam[currentTarget].syncViewOn: #if sync view enable
                        self.syncView()
                else:
                    print("Show on status bar")
            else: #zoom out
                if self.imageParam[currentTarget].zoomValue != WLImageParams.ZOOM_MIN_VALUE:
                    self.imageParam[currentTarget].zoomValue -= 1
                    value = self.imageParam[currentTarget].zoomValue
                    self.mainUi.zoomValue.setText("(x" + str(value) + ")")
                    self.imageParam[currentTarget].imageHolder.makeCurrent()
                    self.imageParam[currentTarget].imageHolder.zoomImage(value + 1,value)
                    if self.imageParam[currentTarget].syncViewOn: #if sync view enable
                        self.syncView()
                else:
                    print("Show on status bar")

    #DESCRIPTION: Zoom image to to destination value (with key value given)
    def zoomImageToRatio(self,key,destinationValue):
        if self.imageParam[key].imageOn:
            self.imageParam[key].imageHolder.makeCurrent()
            self.imageParam[key].imageHolder.zoomImage(self.imageParam[key].zoomValue,destinationValue)
            self.imageParam[key].zoomValue = destinationValue

    #DESCRIPTION: Open magnification diaglog
    def openMagnificationMode(self):
        if self.targetImage is not None:
            self.magnificationDialog = WLMagnificationDialog(self,self.targetImage)
            self.magnificationDialog.showFullScreen()
        else:
            print("No target given")

    def setupBinarizeAction(self):
        if self.targetImage is not None:
            checkBoxState = self.mainUi.binarizeCheckBox.isChecked()
            self.mainUi.binarizeSlider.setEnabled(checkBoxState)
            self.imageParam[self.targetImage].binarizeOn = bool(checkBoxState)
            self.centralAction()
        else:
            self.mainUi.binarizeCheckBox.setChecked(False)

    #DESCRIPTION: Setting up sync view before sync (for UI only)
    def setupSyncView(self):
        if self.targetImage is not None:
            self.imageParam[self.targetImage].syncViewOn = self.mainUi.syncViewCheckBox.isChecked()
            self.syncView()

    #DESCRIPTION: Sync other image view when image changed
    def syncView(self, syncSize = True):
        if self.targetImage is not None:
            if self.imageParam[self.targetImage].imageOn:
                #check syncViewCheckBox
                syncViewState = self.imageParam[self.targetImage].syncViewOn
                if syncViewState:
                    scrollArea = self.imageParam[self.targetImage].scrollArea
                    if scrollArea.underMouse() or self.mainUi.zoomInButton.underMouse() or self.mainUi.zoomOutButton.underMouse():
                        #if mouse is in target widget or in zoom button
                        horizontalValue = scrollArea.horizontalScrollBar().sliderPosition()
                        verticalValue = scrollArea.verticalScrollBar().sliderPosition()
                        horizontalRatio = horizontalValue / (self.imageParam[self.targetImage].imageHolder.size().width() + \
                            scrollArea.horizontalScrollBar().pageStep() - scrollArea.size().width())
                        verticalRatio = verticalValue / (self.imageParam[self.targetImage].imageHolder.size().height() + \
                            scrollArea.verticalScrollBar().pageStep() - scrollArea.size().height())
                        #update other image
                        for key in objectName:
                            if key != self.targetImage and self.imageParam[key].imageOn:
                                updateScrollArea = self.imageParam[key].scrollArea
                                if syncSize:
                                    self.zoomImageToRatio(key,self.imageParam[self.targetImage].zoomValue)
                                updateScrollArea = self.imageParam[key].scrollArea
                                horizontalMaxValue = self.imageParam[key].imageHolder.size().width() + \
                                    updateScrollArea.horizontalScrollBar().pageStep() - updateScrollArea.size().width()
                                verticalMaxValue = self.imageParam[key].imageHolder.size().height() + \
                                    updateScrollArea.verticalScrollBar().pageStep() - updateScrollArea.size().height()
                                updateScrollArea.horizontalScrollBar().setValue(horizontalRatio * horizontalMaxValue)
                                updateScrollArea.verticalScrollBar().setValue(verticalRatio * verticalMaxValue)

    def centralAction(self):
        if self.targetImage is not None:
            target = self.imageParam[self.targetImage].image
            binarizeThreshold = self.mainUi.binarizeSlider.value()
            self.imageParam[self.targetImage].binarizeValue = binarizeThreshold
            valueLabel = self.mainUi.binarizeValue
            if self.imageParam[self.targetImage].binarizeOn == True:
                tmp = binarizeThreshold
                valueLabel.setText(str(tmp).zfill(3))
                result = target.binarize(binarizeThreshold)
            self.imageParam[self.targetImage].imageHolder.makeCurrent()
            self.imageParam[self.targetImage].imageHolder.updateImage()
        #undo redo

    #DESCRIPTION: filtering event from keyboard, or mouse
    def eventFilter(self, source, event):
        #a. If left pressed -> initialize drag mode.
        if event.type() == QtCore.QEvent.MouseButtonPress:
            if event.button() == QtCore.Qt.LeftButton:
                for key in objectName:
                    if self.imageParam[key].imageOn:
                        scrollArea = self.imageParam[key].scrollArea
                        if source is scrollArea.viewport():
                            self.imageParam[key].dragModeOn = True
                            self.lastDragPosition = event.pos()
                            print("Drag Mode In")
                            return True
                return False

        #a. If mouse is hover (which means it is being pressed), drag the image.
        if event.type() == QtCore.QEvent.HoverMove:
            for key in objectName:
                if self.imageParam[key].imageOn:
                    scrollArea = self.imageParam[key].scrollArea
                    viewFrame = self.imageParam[key].viewFrame
                    if source is viewFrame:
                        if self.imageParam[key].dragModeOn:
                            delta = (event.pos() - self.lastDragPosition) / self.DELTA_DIV
                            scrollArea.horizontalScrollBar().setValue(scrollArea.horizontalScrollBar().value() - delta.x())
                            scrollArea.verticalScrollBar().setValue(scrollArea.verticalScrollBar().value() - delta.y())    
                            self.lastDragPosition = event.pos()
                            print("Drag moving")
                        return True
            return False

        #a. If left mouse released, exit drag mode.
        if event.type() == QtCore.QEvent.MouseButtonRelease:
            if event.button() == QtCore.Qt.LeftButton:
                for key in objectName:
                    if self.imageParam[key].imageOn:
                        if source is self.imageParam[key].scrollArea.viewport():
                            self.imageParam[key].dragModeOn = False
                            print("Drag mode out")
                            return True
                return False

        #a. If mouse leaves from image -> enable scroll bar
        #b. If mouse leaves from image -> exit drag mode
        if event.type() == QtCore.QEvent.Leave:
            for key in objectName:
                if self.imageParam[key].imageOn:
                    if source is self.imageParam[key].viewFrame or source is self.imageParam[key].scrollArea.viewport():
                        print("Enable")
                        self.mainUi.mainScrollArea.verticalScrollBar().setEnabled(True)
                        self.imageParam[key].dragModeOn = False
                        return True
            return False

        #a. If mouse enters and under image -> disable vertical scroll bar
        if event.type() == QtCore.QEvent.MouseMove:
            for key in objectName:
                if self.imageParam[key].imageOn:
                    if source is self.imageParam[key].viewFrame or source is self.imageParam[key].scrollArea.viewport():
                        print("Disable")
                        self.mainUi.mainScrollArea.verticalScrollBar().setEnabled(False)
                        return True
            return False

        #a. If double click left mouse -> choose the targeted image
        if event.type() == QtCore.QEvent.MouseButtonDblClick:
            if event.button() == QtCore.Qt.LeftButton:
                #Highlight image
                viewWidgetFlags = [False] * 5
                if source is self.mainUi.bottomBFViewFrame or \
                    source is self.imageParam[self.BOTTOM_BF_IMAGE].scrollArea.viewport() :
                    if self.targetImage is not self.BOTTOM_BF_IMAGE:
                        self.mainUi.bottomBFViewFrame.setStyleSheet("QFrame{background-color: rgb(51,60,53);}")
                        viewWidgetFlags[0] = True
                        self.updateStatus(self.BOTTOM_BF_IMAGE)
                if source is self.mainUi.bottomRingViewFrame or \
                    source is self.imageParam[self.BOTTOM_RING_IMAGE].scrollArea.viewport() :
                    if self.targetImage is not self.BOTTOM_RING_IMAGE:
                        self.mainUi.bottomRingViewFrame.setStyleSheet("QFrame{background-color: rgb(51,60,53);}")
                        viewWidgetFlags[1] = True
                        self.updateStatus(self.BOTTOM_RING_IMAGE)
                if source is self.mainUi.topUpperViewFrame or \
                    source is self.imageParam[self.TOP_UPPER_IMAGE].scrollArea.viewport() :
                    if self.targetImage is not self.TOP_UPPER_IMAGE:
                        self.mainUi.topUpperViewFrame.setStyleSheet("QFrame{background-color: rgb(51,60,53);}")
                        viewWidgetFlags[2] = True
                        self.updateStatus(self.TOP_UPPER_IMAGE)
                if source is self.mainUi.bottomDFViewFrame or \
                    source is self.imageParam[self.BOTTOM_DF_IMAGE].scrollArea.viewport() :
                    if self.targetImage is not self.BOTTOM_DF_IMAGE:
                        self.mainUi.bottomDFViewFrame.setStyleSheet("QFrame{background-color: rgb(51,60,53);}")
                        viewWidgetFlags[3] = True
                        self.updateStatus(self.BOTTOM_DF_IMAGE)
                if source is self.mainUi.bottomPowerViewFrame or \
                    source is self.imageParam[self.BOTTOM_POWER_IMAGE].scrollArea.viewport() :
                    if self.targetImage is not self.BOTTOM_POWER_IMAGE:
                        self.mainUi.bottomPowerViewFrame.setStyleSheet("QFrame{background-color: rgb(51,60,53);}")
                        viewWidgetFlags[4] = True
                        self.updateStatus(self.BOTTOM_POWER_IMAGE)
                #Unhighlight other images
                if viewWidgetFlags[0] == False:
                    self.mainUi.bottomBFViewFrame.setStyleSheet("QFrame{background-color: rgb(31,40,33);} \
                    QFrame#bottomBFViewFrame:hover{background-color: rgb(36,45,38);}")
                if viewWidgetFlags[1] == False:
                    self.mainUi.bottomRingViewFrame.setStyleSheet("QFrame{background-color: rgb(31,40,33);} \
                    QFrame#bottomRingViewFrame:hover{background-color: rgb(36,45,38);}")
                if viewWidgetFlags[2] == False:
                    self.mainUi.topUpperViewFrame.setStyleSheet("QFrame{background-color: rgb(31,40,33);} \
                    QFrame#topUpperViewFrame:hover{background-color: rgb(36,45,38);}")
                if viewWidgetFlags[3] == False:
                    self.mainUi.bottomDFViewFrame.setStyleSheet("QFrame{background-color: rgb(31,40,33);} \
                    QFrame#bottomDFViewFrame:hover{background-color: rgb(36,45,38);}")
                if viewWidgetFlags[4] == False:
                    self.mainUi.bottomPowerViewFrame.setStyleSheet("QFrame{background-color: rgb(31,40,33);} \
                    QFrame#bottomPowerViewFrame:hover{background-color: rgb(36,45,38);}")
                #update current status
                if True not in viewWidgetFlags:
                    self.updateStatus(None)
                return True
            return False

        #a. If using wheel and mouse under an image -> zoom image
        if event.type() == QtCore.QEvent.Wheel:
            for key in objectName:
                if self.imageParam[key].imageOn:
                    scrollArea = self.imageParam[key].scrollArea
                    if source is scrollArea.viewport():
                        if abs(event.angleDelta().y()) > self.MOUSE_DEGREE_THRESHOLD:
                            #Prevent MainWindow scroll area
                            if event.angleDelta().y() > 0: #zoom in
                                self.zoomImage(True,key)
                            if event.angleDelta().y() < 0: #zoom out
                                self.zoomImage(False,key)
                            return True
            return False
        
        return super().eventFilter(source,event)

    #DECSRIPTION: resize images when Main window resize
    def resizeEvent(self,event):
        #resize widgets
        QtWidgets.QApplication.processEvents()
        for key in objectName:
            if self.imageParam[key].imageOn:
                self.imageParam[key].imageHolder.update()
                scrollArea = self.imageParam[key].scrollArea
                scrollArea.setFixedHeight(self.imageParam[key].imageHolder.size().height() * self.DUMP_RATIO)    
        return super().resizeEvent(event)
    
    #Update UI when self.targetImage is changed
    def updateStatus(self,target):
        self.targetImage = target
        if target is not None and self.imageParam[self.targetImage].imageOn:
            #Change message
            #enable menu function
            self.ui.actionBinarize.setEnabled(True)
            self.ui.actionOpenMagnificationMode.setEnabled(True)
            self.ui.actionZoomIn.setEnabled(True)
            self.ui.actionZoomOut.setEnabled(True)
            self.ui.actionSyncView.setEnabled(True)
            self.mainUi.binarizeCheckBox.setEnabled(True)
            self.mainUi.magnificationButton.setEnabled(True)
            self.mainUi.zoomInButton.setEnabled(True)
            self.mainUi.zoomOutButton.setEnabled(True)
            self.mainUi.syncViewCheckBox.setEnabled(True)
            self.mainUi.binarizeSlider.setEnabled(self.imageParam[self.targetImage].binarizeOn)
            self.ui.statusBar.showMessage("Target: " + sysName[target])
            tmp = self.imageParam[self.targetImage].binarizeValue
            self.mainUi.binarizeValue.setText(str(tmp).zfill(3))
            zoomValue = self.imageParam[self.targetImage].zoomValue
            self.mainUi.zoomValue.setText("(x" + str(zoomValue) + ")")
            self.mainUi.binarizeSlider.setValue(self.imageParam[self.targetImage].binarizeValue)
            self.mainUi.binarizeCheckBox.setChecked(self.imageParam[self.targetImage].binarizeOn)
            self.mainUi.syncViewCheckBox.setChecked(self.imageParam[self.targetImage].syncViewOn)
            #Update binarize value
        else:
            #disable menu functions
            self.ui.openImageAction.setEnabled(False)
            self.ui.actionBinarize.setEnabled(False)
            self.ui.actionSyncView.setEnabled(False)
            self.ui.actionOpenMagnificationMode.setEnabled(False)
            self.ui.actionZoomIn.setEnabled(False)
            self.ui.actionZoomOut.setEnabled(False)
            self.mainUi.binarizeCheckBox.setChecked(False)
            self.mainUi.syncViewCheckBox.setChecked(False)
            self.ui.statusBar.showMessage("Target: None")
            self.mainUi.binarizeValue.setText("000")
            self.mainUi.zoomValue.setText("")
            self.mainUi.binarizeSlider.setValue(0)
            self.mainUi.binarizeCheckBox.setEnabled(False)
            self.mainUi.binarizeSlider.setEnabled(False)
            self.mainUi.magnificationButton.setEnabled(False)
            self.mainUi.zoomInButton.setEnabled(False)
            self.mainUi.zoomOutButton.setEnabled(False)
            self.mainUi.syncViewCheckBox.setEnabled(False)
        #update UI
        if self.targetImage is not None:
            self.ui.statusBar.showMessage("Target: " + sysName[target])
            self.ui.openImageAction.setEnabled(True)
