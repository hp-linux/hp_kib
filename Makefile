#Compiler
UICPL = pyuic5
RCCPL = pyrcc5
#Add import for resources
RCIMPFLAG = --import-from=forms.resources
#Root path
RCROOTPATH = forms/resources
MAINFILEUIROOTPATH = forms/main
MAINFILEPYROOTPATH = forms/main/forms_py
MENUFILEUIROOTPATH = forms/menu_objects/menu_file_objects
MENUFILEPYROOTPATH = forms/menu_objects/menu_file_objects/forms_py
CXX_MODULES = cxx_modules
CLEAN_CMAKE_CXX_LIB_COMMAND = rm -r CMakeFiles ; rm cmake_install.cmake ; rm CMakeCache.txt ; rm Makefile
default: clean begin start end
run: clean begin start end
build: clean begin end

clean:
	#Create forms_py fodlers if not existed
	mkdir -p $(MAINFILEPYROOTPATH)
	mkdir -p $(MENUFILEPYROOTPATH)
	#Clean .py file of ui
	rm -f $(MENUFILEPYROOTPATH)/*.py
	rm -f $(MAINFILEPYROOTPATH)/*.py
	rm -f $(RCROOTPATH)/*.py
begin:
	#building C++ library
	#WLObject
	cd $(CXX_MODULES) && cmake . -DPYTHON_DESIRED_VERSION=3.X; make; $(CLEAN_CMAKE_CXX_LIB_COMMAND)
	#WLFunc
	#1. Go to cxx_libs
	#Compiling resource files
	$(RCCPL) $(RCROOTPATH)/resource.qrc > $(RCROOTPATH)/resource_rc.py
	#Compiling UI components
	$(UICPL) $(RCIMPFLAG) -x $(MAINFILEUIROOTPATH)/MainWidget.ui > $(MAINFILEPYROOTPATH)/MainWidget.py
	$(UICPL) $(RCIMPFLAG) -x $(MAINFILEUIROOTPATH)/MainWindow.ui > $(MAINFILEPYROOTPATH)/MainWindow.py
	$(UICPL) $(RCIMPFLAG) -x $(MAINFILEUIROOTPATH)/MagnificationDialog.ui > $(MAINFILEPYROOTPATH)/MagnificationDialog.py
	$(UICPL) -x $(MENUFILEUIROOTPATH)/OpenDialog.ui > $(MENUFILEPYROOTPATH)/OpenDialog.py
start:
	python3 main.py
end: