from cxx_modules.WLFunc import WLEdgeDetector, WLFilter, WLSeg
from cxx_modules import WLFunc
from cxx_modules.WLObject import *
from PIL import Image

firstImage = WLImage("./bottom_bf.bmp",WLImage.GRAYSCALE)
secondImage = WLImage()
region = WLRegion()


#result = WLEdgeDetector.sobelAmplitude(firstImage,secondImage, \
    #WLEdgeDetector.SUM_ABS,WLEdgeDetector.GAUSSIAN_FILTER,11)
WLFilter.binomialFilter(firstImage,secondImage,11,11)
#result = WLEdgeDetector.sobelAmplitude(firstImage,secondImage, \
    #WLEdgeDetector.SUM_ABS,WLEdgeDetector.BINOMIAL_FILTER,11)
#result = WLSeg.hysteresisThreshold(firstImage,region,195,200,40)
#WLRegion test

#result = WLEdgeDetector.sobelAmplitude(firstImage,secondImage, \
    #WLEdgeDetector.SUM_SQRT,WLEdgeDetector.BINOMIAL_FILTER,11)

#result = WLSeg.threshold(firstImage,region,190)
#result = WLSeg.thresholdWithBoundary(secondImage,region,10,20)
#result = WLSeg.hysteresisThreshold(firstImage,region,190,200,10)
'''
region.applyOnImage(secondImage,secondImage)
width = secondImage.getWidth()
height = secondImage.getHeight()
im = Image.frombytes('RGB',(width,height),secondImage.getImageBytes(),'raw')
im.save("t.bmp")
a = region.size()
for i in range(a):
    secondImage = WLImage()
    region.createWLImage(secondImage,i)
    width = secondImage.getWidth()
    height = secondImage.getHeight()
    im = Image.frombytes('RGB',(width,height),secondImage.getImageBytes(),'raw')
    im.save("ts" + str(i) + '.bmp')
'''
#Divided component tests
'''
result = WLEdgeDetector.sobelAmplitude(firstImage,secondImage, \
    WLEdgeDetector.SUM_SQRT,WLEdgeDetector.BINOMIAL_FILTER,11)
for i in range(15):
    result = WLSeg.hysteresisThreshold(secondImage,region,40,60,2)
a = region.size()
'''
#WLSeg.divideConnectedRegion(region,region,-1,4)
#region.applyOnImage(firstImage,firstImage,-1)
width = secondImage.getWidth()
height = secondImage.getHeight()
byteImage = None
im = Image.frombytes('RGB',(width,height),secondImage.getImageBytes(),'raw')
im.show()